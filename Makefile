BASE_DIR ?= $(shell pwd)
SRC_DIR = $(BASE_DIR)/src
DATA_DIR = $(BASE_DIR)/data
WORK_DIR = $(BASE_DIR)/work

SYZKALLER_GIT_URL ?= https://github.com/google/syzkaller.git
SYZKALLER_GIT_BRANCH ?= master
SYZKALLER_PATCHES = $(wildcard $(DATA_DIR)/patches/*.patch)

KERNEL_GIT_URL ?= https://github.com/torvalds/linux.git
KERNEL_GIT_BRANCH ?= master
KERNEL_SRC_DIR ?= $(SRC_DIR)/linux

CCACHE := $(shell command -v ccache)

ifdef CCACHE
    CC := ccache gcc
    export CCACHE_DIR = $(BASE_DIR)/.ccache
else
    CC := gcc
endif

VM_INSTALL_PKGS = "openssh-server,curl,tar,gcc,libc6-dev,time,strace,sudo,less,psmisc,selinux-utils,policycoreutils,checkpolicy,selinux-policy-default,firmware-atheros,debian-ports-archive-keyring,make,sysbench,git,vim,tmux,usbutils,tcpdump,watchdog"


download_syzkaller: $(SRC_DIR)/syzkaller

$(SRC_DIR)/syzkaller:
	git clone --depth 1 --branch $(SYZKALLER_GIT_BRANCH) $(SYZKALLER_GIT_URL) $(SRC_DIR)/syzkaller

build_syzkaller_container:
	podman build -t syzkaller-env:latest -f $(DATA_DIR)/build_env/Containerfile

apply_syzkaller_patches: $(SYZKALLER_PATCHES)
	@pushd $(SRC_DIR)/syzkaller > /dev/null;	\
	for PATCH in "$^"; do \
		if git apply --check "$$PATCH"; then \
			git apply "$$PATCH"; \
		fi; \
	done; \
	popd > /dev/null;

build_syzkaller: $(SRC_DIR)/syzkaller/bin/syz-manager

$(SRC_DIR)/syzkaller/bin/syz-manager: download_syzkaller apply_syzkaller_patches build_syzkaller_container
	podman run --rm -v $(SRC_DIR)/syzkaller:/go/src/syzkaller:Z -t syzkaller-env:latest make

clean_syzkaller:
	if [ -d $(SRC_DIR)/syzkaller]; \
	then \
		pushd $(SRC_DIR)/syzkaller > /dev/null;	\
		make clean; \
		popd > /dev/null; \
	fi


download_linux: $(SRC_DIR)/linux

$(SRC_DIR)/linux:
	git clone --depth 1 --branch $(KERNEL_GIT_BRANCH) $(KERNEL_GIT_URL) $(SRC_DIR)/linux

build_linux: download_linux $(SRC_DIR)/linux/.config
	pushd $(SRC_DIR)/linux > /dev/null; \
	make CC="$(CC)" -j`nproc`; \
	popd > /dev/null;

$(SRC_DIR)/linux/.config:
	pushd $(SRC_DIR)/linux > /dev/null; \
	make CC="$(CC)" defconfig; \
	make CC="$(CC)" kvm_guest.config; \
	sed -e "s/^[# ]*CONFIG_KCOV[= ].*$$/CONFIG_KCOV=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_DEBUG_INFO_DWARF4[= ].*$$/CONFIG_DEBUG_INFO_DWARF4=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_KASAN[= ].*$$/CONFIG_KASAN=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_KASAN_INLINE[= ].*$$/CONFIG_KASAN_INLINE=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_CONFIGFS_FS[= ].*$$/CONFIG_CONFIGFS_FS=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_SECURITYFS[= ].*$$/CONFIG_SECURITYFS=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_FAULT_INJECTION[= ].*$$/CONFIG_FAULT_INJECTION=y/" -i $(SRC_DIR)/linux/.config; \
	sed -e "s/^[# ]*CONFIG_I6300ESB_WDT[= ].*$$/CONFIG_I6300ESB_WDT=y/" -i $(SRC_DIR)/linux/.config; \
	make CC="$(CC)" olddefconfig; \
	popd > /dev/null;

build_syzkaller_vm: $(BASE_DIR)/syzkaller-vm.qcow2

$(BASE_DIR)/syzkaller-vm.id_rsa:
	ssh-keygen -t rsa -N '' -f "$(BASE_DIR)/syzkaller-vm.id_rsa"

$(BASE_DIR)/syzkaller-vm.qcow2: $(BASE_DIR)/syzkaller-vm.id_rsa
	virt-builder debian-12 -o $(BASE_DIR)/syzkaller-vm.qcow2 \
		--format qcow2 \
		--hostname syzkaller \
		--install $(VM_INSTALL_PKGS) \
		--ssh-inject root:file:$(BASE_DIR)/syzkaller-vm.id_rsa.pub \
		--append-line "/etc/ssh/sshd_config:PermitRootLogin yes" \
		--append-line "/etc/network/interfaces:auto eth0" \
		--append-line "/etc/network/interfaces:iface eth0 inet dhcp" \
		--mkdir /etc/systemd/system/serial-getty@ttyS0.service.d \
		--copy-in "$(DATA_DIR)/vm/autologin.conf":/etc/systemd/system/serial-getty@ttyS0.service.d \
		--root-password password:password

$(BASE_DIR)/syzkaller-vm.cfg:
	@cp -f $(DATA_DIR)/conf/template.conf $(BASE_DIR)/syzkaller-vm.cfg
	@sed -e "s:\$$BASE_DIR:$(BASE_DIR):g" -i $(BASE_DIR)/syzkaller-vm.cfg
	@sed -e "s:\$$WORK_DIR:$(WORK_DIR):g" -i $(BASE_DIR)/syzkaller-vm.cfg
	@sed -e "s:\$$SRC_DIR:$(SRC_DIR):g" -i $(BASE_DIR)/syzkaller-vm.cfg


build: build_syzkaller build_linux build_syzkaller_vm $(BASE_DIR)/syzkaller-vm.cfg

run_vm: $(BASE_DIR)/syzkaller-vm.qcow2
	qemu-system-x86_64 \
		-m 2G \
		-smp 2 \
		-kernel $(SRC_DIR)/linux/arch/x86/boot/bzImage \
		-append "console=ttyS0 root=/dev/sda1 earlyprintk=serial net.ifnames=0" \
		-drive file=$(BASE_DIR)/syzkaller-vm.qcow2,format=qcow2 \
		-netdev user,id=net0,hostfwd=tcp::10021-:22 \
		-device virtio-net-pci,netdev=net0 \
		-device i6300esb,id=watchdog0 -watchdog-action debug \
		-enable-kvm \
		-nographic \
		-pidfile vm.pid \
		2>&1 | tee vm.log

syz_manager: $(BASE_DIR)/syzkaller-vm.cfg
	$(SRC_DIR)/syzkaller/bin/syz-manager -config $(BASE_DIR)/syzkaller-vm.cfg

syz_generate: $(SRC_DIR)/syzkaller $(SRC_DIR)/linux build_syzkaller_container
	podman run --rm -v $(SRC_DIR)/syzkaller:/go/src/syzkaller:Z \
		-v $(SRC_DIR)/linux:/go/src/linux:Z \
		-t syzkaller-env:latest make extract TARGETOS=linux SOURCEDIR=/go/src/linux
	podman run --rm -v $(SRC_DIR)/syzkaller:/go/src/syzkaller:Z \
		-v $(SRC_DIR)/linux:/go/src/linux:Z \
		-t syzkaller-env:latest make generate

clean_linux:
	if [ -d $(SRC_DIR)/linux ]; \
	then \
		pushd $(SRC_DIR)/linux > /dev/null;	\
		make mrproper; \
		popd > /dev/null; \
	fi

clean: clean_linux clean_syzkaller

mrproper:
	rm -fr $(SRC_DIR)/syzkaller
	rm -fr $(SRC_DIR)/linux
	rm -fr $(BASE_DIR)/work
	rm -fr $(BASE_DIR)/.ccache
	rm -f  $(BASE_DIR)/syzkaller-vm.cfg
	rm -f  $(BASE_DIR)/syzkaller-vm.qcow2
	rm -f  $(BASE_DIR)/syzkaller-vm.id_rsa
	rm -f  $(BASE_DIR)/syzkaller-vm.id_rsa.pub

all: build

.PHONY: all build clean mrproper \
	download_syzkaller download_linux \
	build_syzkaller build_syzkaller_container build_linux build_syzkaller_vm \
	apply_syzkaller_patches generate_ssh_keys \
	clean_syzkaller clean_linux
.DEFAULT_GOAL := all
