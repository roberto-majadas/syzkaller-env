# Syzkaller-env

This repo helps to automate the setup, configuration, running, and cleanup processes involved in using Syzkaller, an unsupervised, coverage-guided Linux kernel fuzzer.

## Installation Requirements

You can install all the required packages using the following command:
```bash
$ sudo dnf install -y podman "@Development Tools" qemu libguestfs-tools
```

## Usage

All the follow commands are make commands so you just need to execute `make <command>`

### Syzkaller-related

- `syz_manager`: Runs the syzkaller manager using the syzkaller-vm.cfg configuration file generated by `build_syzkaller_vm`.
- `syz_generate`: Extract and generate information from the linux kernel sources and the new syzkaller descriptions added.

### Build and general commands

- `build`: Executes `build_syzkaller`, `build_linux`, and others
- `build_linux`: Downloads the linux source, configures it, and builds the kernel.
- `build_syzkaller`: Downloads syzkaller, applies patches, builds the Docker container, and builds syzkaller using that container.
- `build_syzkaller_vm`: Generates an RSA key pair, builds a Debian-based virtual machine image, and generates a configuration file for syzkaller.
- `run_vm`: Runs the virtual machine image built by `build_syzkaller_vm` (for debugging purposes)

### Clean commands

- `clean`: Executes `clean_linux` and `clean_syzkaller`.
- `clean_linux`: Cleans the linux source directory if it exists.
- `clean_syzkaller`: Cleans the syzkaller source directory if it exists.
- `mrproper`: Deletes source directories and removes certain generated files and directories.

## Example

```bash
$ make build
...
$ make syz_manager
2024/06/17 11:48:05 serving http on http://127.0.0.1:56741
2024/06/17 11:48:05 serving rpc on tcp://[::]:42065
2024/06/17 11:48:05 booting test machines...
2024/06/17 11:48:05 wait for the connection from test machine...
2024/06/17 11:48:34 machine check:
...

```